/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.overloadmethod;

/**
 *
 * @author nunezd
 */
public class OverloadingMethodNames {

    /**
     *
     * @param args this is the main method of this project
     */
    public static void main(String[] args) {

        int value1 = 5;
        int value2 = 10;
        int value3 = 15;

        int result = addValues(value1, value2, value3);
        System.out.println("The result is " + result);
        
        String string1 = "10";
        String string2 = "25";
        int result2 = addValues(string1, string2);
        System.out.println("The result is " + result2);

    }

    private static int addValues(int int1, int int2) {
        return int1 + int2;
    }
    
    private static int addValues(int int1, int int2, int int3) {
        return int1 + int2 + int3;
    }
    
    //You can have more than one method of the same name as long as they have different numbers of arguments i.e. addValues
    //You can also distinguish methods from each other by the data types of the arguments
    
    private static int addValues(String val1, String val2) {
        int value1 = Integer.parseInt(val1);
        int value2 = Integer.parseInt(val2);
        return value1 + value2;
    }
}

//When you overload a method, you are reusing the same method name, but you're creating
//alternative signatures. This is good coding strategy in Java to handle situations
//where in one circumstance you have a certain set of variables, in another circumstance
//you got another set and where the data types can differ as well.